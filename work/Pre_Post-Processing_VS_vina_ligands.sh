#!/bin/bash
# jion78 #


###### Pre_Processing ######

# Transforming the Receptor
             /home/jion78/Downloads/mgltools_x86_64Linux2_1.5.6/bin/pythonsh /home/jion78/Desktop/work/Dock_pgms/transform-receptor.py receptor.pdb  -14.282  -8.151  63.568

#Transforming the ligands
               cd ligand
               ls lig*.pdb > lig_list
               perl /home/jion78/Desktop/work/Dock_pgms/transform-ligand.pl lig_list
               cd ..

#Preparing the receptor
               /home/jion78/Downloads/mgltools_x86_64Linux2_1.5.6/bin/pythonsh /home/jion78/Desktop/work/Dock_pgms/prepare_receptor4.py -r receptor_transformed.pdb -U ‘’ -A ‘checkhydrogens’

#Preparing the ligands
	       cd ligand
               ls lig*_transformed.pdb > trans_list
               perl /home/jion78/Desktop/work/Dock_pgms/prepare_ligand.pl
               ls *transformed.pdbqt > pdbqt_list
	       cd ..
#### Post_Processing ####

#Autodock Vina Docking
               mkdir vina_ligand
               cp receptor_transformed.pdbqt vina_ligand/
               mkdir vina_ligand/lig
               cp ligand/lig*_transformed.pdbqt vina_ligand/lig
               cd vina_ligand
               sh /home/jion78/Desktop/work/Dock_pgms/vina_VS_input_final.sh ../receptor_transformed.pdbqt lig/ 30 30 30
               cd ligand/
               ls -1 >list1
               sed -i '1i Molecules' list1
               sed '$d' list1 >list
               mkdir active
               perl //home/jion78/Desktop/work/Dock_pgms/vina_split.pl list
               cp ../../receptor_transformed.pdbqt active/
               cd active/
               ls *.pdbqt >pdbqt_list
               perl /home/jion78/Desktop/work/Dock_pgms/prepare_pdbqt_to_pdb.pl
               cd ..

#Vina_energy

               ls | grep lig > list_vina
               perl /home/jion78/Desktop/work/Dock_pgms/parse.pl list_vina
